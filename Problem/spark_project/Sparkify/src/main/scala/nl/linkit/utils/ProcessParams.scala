package nl.linkit.utils

import org.rogach.scallop.ScallopConf

class ProcessParams(arguments: Seq[String]) extends ScallopConf(arguments) {
  val env = opt[String]()

  // Run argument extractions
  verify()

  override def toString: String = s"ProcessParams(${arguments.mkString(" ")})"
}
